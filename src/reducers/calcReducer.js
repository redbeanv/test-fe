export const CALC_ACTION = {
  SUM_VALUE: "SUM_VALUE",
};

export const initialState = {
  sumValue: 0,
};

const calcReducer = (state = initialState, action) => {
  switch (action.type) {
    case CALC_ACTION.SUM_VALUE:
      return {
        ...state,
        sumValue: action.sumValue
      }
    default:
      return state
  }
}

export default calcReducer;
