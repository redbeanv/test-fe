import React from 'react';
import { Provider } from 'react-redux';
import store from './stores/configureStores';

import Account from './components/accountBook';
import './App.css';

function App() {
  return (
    <Provider store={store}>
      <Account />
    </Provider>
  );
}

export default App;
