# Front End(React) Test

## project start

`npm run start`
   
<hr />

## Description

![image](./img/1.png)    

### 기본 기능 정리
1. __+버튼__ : AccountInput 추가
2. __remove버튼__ : AccountInput 삭제
3. __합계__ : 추가된 인풋의 value값을 합을 '합계: ***'로 표시

### 추가 기능
1. 합계의 값을 state가 아닌 redux의 store값으로 변경 및 action 처리로 변경