import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { CALC_ACTION } from '../../reducers/calcReducer';
import AccountInput from './AccountInput';

const Wrapper = styled.div`
  width: 600px;
  margin: 0 auto;
`

const Title = styled.div`
  background: black;
  border: 1px solid black;
  border-top-left-radius: 16px;
  border-top-right-radius: 16px;
  color: white;
  padding: 16px;
  width: 450px;
  margin: 0 auto;
  margin-top: 50px;
  & > h2 {
    margin: 0;
    display: flex;
    justify-content: center;
  }
`

const InputContainer = styled.div`
  border: 2px solid black;
  border-radius: 16px;
  padding: 16px;
  margin: 0 auto;
`

const Divider = styled.hr`
  margin-top: 16px;
  margin-bottom: 16px;
  border: 0;
  border-top: 2px solid black;
`

const Summary = styled.div`
  display: flex;
  justify-content: flex-end;
  font-size: 1.17em;
  margin-block-start: 1em;
  margin-block-end: 1em;
  margin-inline-start: 0px;
  margin-inline-end: 0px;
  font-weight: bold;
`

const Action = styled.div`
  padding-top: 8px;
  height: 48px;
  display: flex;
  justify-content: flex-end;
`

function Account() {
  const [inputList, setInputList] = useState([]);
  // const [sumValue, setSumValue] = useState(0);
  const sumValue = useSelector(state => state.calc.sumValue);
  const dispatch = useDispatch();

  const addInput = () => {
    const list = [...inputList];
    setInputList([...list, { amount: 0 }]);
  };

  const removeInput = (index) => {
    const list = [...inputList];
    list.splice(index, 1);
    setInputList(list);
  };

  const onSum = () => {
    const result = inputList.reduce((acc, curr) => {
      return acc + curr.amount
    }, 0);
    // setSumValue(result);
    dispatch({ type: CALC_ACTION.SUM_VALUE, sumValue: result });
  };

  return (
    <Wrapper>
      
      <Title>
        <h2>Account Book</h2>
      </Title>

      <InputContainer>
        {inputList.map((inputNode, index) => {
          return (
            <AccountInput
              key={index}
              index={index}
              inputNode={inputNode}
              onRemove={removeInput}
              inputList={inputList}
              setList={setInputList}
            />
          )
        })}

        <Divider />

        <Summary>
          {`합계 : ${sumValue}`}
        </Summary>

        <Action>
          <button onClick={addInput}>+</button>
          <button onClick={onSum}>합계</button>
        </Action>

      </InputContainer>
    </Wrapper>
  );
}

export default Account;
