import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  height: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 8px;
  padding-left: 8px;
  padding-right: 8px;
`

const ContentIndex = styled.h4`
  min-width: 24px;
`

const ContentInput = styled.input`
  height: 20px;
  margin-left: 8px;
  width: 100%;
  padding: 8px;
`

function AccountInput({ index, inputNode, onRemove, inputList, setList }) {
  const changeAmount = (event, index) => {
    const { name, value } = event.target;
    const list = [...inputList];
    list[index][name] = Number(value);
    setList(list);
  };

  return (
    <Wrapper>
      <ContentIndex>{`${index}.`}</ContentIndex>
      <ContentInput
        name='amount'
        type='number'
        value={inputNode.amount}
        onChange={(event) => changeAmount(event, index)}
      />
      <button onClick={() => onRemove(index)}>remove</button>
    </Wrapper>
  );
}

export default AccountInput;
